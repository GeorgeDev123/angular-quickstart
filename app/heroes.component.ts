import { Component } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from './hero.services';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'my-app',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
  providers: [HeroService]
})

export class HeroesComponent implements OnInit { 
  constructor (private heroService: HeroService, private router: Router){ }
  heroes: Hero[];
  title = 'tour of Heroes';
  selectedHero: Hero;
  ngOnInit(): void{
    this.getHeroes();
  }
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
  getHeroes(): void {
    this.heroService.getHeroes().then(heroes => this.heroes = heroes);
  }
  gotoDetail(): void{
    this.router.navigate(['/detail', this.selectedHero.id])
  }
}