import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HeroDetailComponent } from './hero-detail.component';
import { HeroesComponent } from './heroes.component';
import { HeroService } from './hero.services';
import { DashboardComponent } from './dashboard.component';

import { RouterModule } from '@angular/router'

import { AppRoutingModule } from './app-routing.module';

import { AppComponent }  from './app.component';

@NgModule({
  imports: [ BrowserModule, FormsModule, AppRoutingModule ],
  declarations: [ AppComponent, HeroDetailComponent, HeroesComponent, DashboardComponent ],
  providers: [ HeroService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }